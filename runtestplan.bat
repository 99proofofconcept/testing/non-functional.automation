@echo off

Rem Local system variables
set jmeterBinary="C:\apache-jmeter-5.3\bin\jmeter"

Rem Plan name variable with extension
set planTestName="testplan.jmx"

Rem Result file name
set nameResultFile="testplan.jtl"

Rem Folder Html result
set folderReport="reports/"

Rem Renove folder report before to generate it again
rmdir /s /q %folderReport%

Rem Run test and write a result data file jtl
call %jmeterBinary% -n -t %planTestName% -l %nameResultFile%

Rem Generate html report
call %jmeterBinary% -g %nameResultFile% -o %folderReport%