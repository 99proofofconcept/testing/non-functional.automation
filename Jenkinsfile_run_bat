pipeline {

    options {
        buildDiscarder(logRotator(numToKeepStr: '10')) 
        ansiColor('xterm')
        timestamps()
		skipDefaultCheckout(true)
    }

    agent any

    stages {

        stage('Checkout'){
		
           steps {
				cleanWs()
                checkout scm
            }
        }

        stage('Execute Script'){

            options{
                timeout(time: 10, unit: "MINUTES")
            }

            steps {
               bat 'runtestplan.bat'
            }
        }
		
		stage('Archive artifacts'){

            steps{
                zip zipFile: 'report.zip', dir: 'reports', overwrite: true
                archiveArtifacts artifacts: '*.jtl,*.zip', fingerprint: true

            }
        }

        stage('Publish Performance'){

            steps{
                perfReport filterRegex: '', relativeFailedThresholdNegative: 1.2, relativeFailedThresholdPositive: 1.89, relativeUnstableThresholdNegative: 1.8, relativeUnstableThresholdPositive: 1.5, sourceDataFiles: 'testplan.jtl'
            }
        }
    }
	
	post {
        success {
            publishHTML(
                [
                    allowMissing: false, 
                    alwaysLinkToLastBuild: true, 
                    keepAll: true, 
                    reportDir: 'reports', 
                    reportFiles: 'index.html', 
                    reportName: 'HTML Report', 
                    reportTitles: ''
                ]
            )
        }
    }
}