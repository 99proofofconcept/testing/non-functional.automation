pipeline {

    options {
        buildDiscarder(logRotator(numToKeepStr: '10')) 
        ansiColor('xterm')
        timestamps() 
        skipDefaultCheckout(true)
    }

    agent any

    environment{
        JMETER_BINARY = "C:/apache-jmeter-5.3/bin/jmeter"
        PLAN_TESTNAME = "testplan.jmx"
        RESULT_FILENAME = "testplan.jtl"
        FOLDER_REPORT = "reports/"
    }

    stages {

        stage('Checkout'){
		
           steps {
                cleanWs()
                checkout scm
            }
        }
   
      
        stage('Execute Test plan'){

            options{
                timeout(time: 10, unit: "MINUTES")
            }

            steps {
                bat label: '', script: 'call %JMETER_BINARY% -n -t %PLAN_TESTNAME% -l %RESULT_FILENAME%'
            }
        }

        stage('Generate HTML report'){

             options{
                timeout(time: 5, unit: "MINUTES")
            }

            steps {
                bat label: '', script: 'call %JMETER_BINARY% -g %RESULT_FILENAME% -o %FOLDER_REPORT%'
            }
        }

         stage('Archive artifacts'){
		 
            steps{
                zip zipFile: 'report.zip', dir: 'reports', overwrite: true
                archiveArtifacts artifacts: '*.jtl,*.zip', fingerprint: true

            }
        }

        stage('Publish Performance'){

            steps{
                perfReport filterRegex: '', relativeFailedThresholdNegative: 1.2, relativeFailedThresholdPositive: 1.89, relativeUnstableThresholdNegative: 1.8, relativeUnstableThresholdPositive: 1.5, sourceDataFiles: 'testplan.jtl'
            }
        }
    }

    post {
        success {
            publishHTML(
                [
                    allowMissing: false, 
                    alwaysLinkToLastBuild: true, 
                    keepAll: true, 
                    reportDir: 'reports', 
                    reportFiles: 'index.html', 
                    reportName: 'HTML Report', 
                    reportTitles: ''
                ]
            )
        }
    }
}